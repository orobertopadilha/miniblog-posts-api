package br.edu.unisep.miniblog.controller;

import br.edu.unisep.miniblog.domain.dto.NewPostDto;
import br.edu.unisep.miniblog.domain.dto.PostDto;
import br.edu.unisep.miniblog.domain.usecase.GetPostsByUserUseCase;
import br.edu.unisep.miniblog.domain.usecase.SavePostUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/posts")
@RequiredArgsConstructor
public class PostController {

    private final GetPostsByUserUseCase getPostsByUser;
    private final SavePostUseCase savePost;

    @PostMapping
    public ResponseEntity save(@RequestBody NewPostDto newPost, @RequestHeader("auth-user-id") Integer userId) {
        savePost.execute(newPost, userId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{userId}")
    public ResponseEntity<List<PostDto>> findByUser(@PathVariable("userId") Integer userId) {
        var posts = getPostsByUser.execute(userId);
        return ResponseEntity.ok(posts);
    }


    @GetMapping
    public ResponseEntity<List<PostDto>> findMyPosts(@RequestHeader("auth-user-id") Integer userId) {
        return findByUser(userId);
    }

}
