package br.edu.unisep.miniblog.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_post")
    private Integer id;

    @Column(name="id_user")
    private Integer user;

    @Column(name="content")
    private String content;

    @Column(name="date")
    private LocalDateTime date;

}
