package br.edu.unisep.miniblog.domain.dto;

import lombok.Data;

@Data
public class NewPostDto {

    private String content;

}
