package br.edu.unisep.miniblog.domain.usecase;

import br.edu.unisep.miniblog.domain.builder.PostBuilder;
import br.edu.unisep.miniblog.domain.dto.PostDto;
import br.edu.unisep.miniblog.model.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GetPostsByUserUseCase {

    private final GetUserNameUseCase getUserName;
    private final PostRepository postRepository;
    private final PostBuilder builder;

    public List<PostDto> execute(Integer userId) {
        var userName = getUserName.execute(userId);
        var posts = postRepository.findByUserOrderByDateDesc(userId);
        return builder.from(posts, userName);
    }

}
