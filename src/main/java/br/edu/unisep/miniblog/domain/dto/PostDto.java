package br.edu.unisep.miniblog.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class PostDto {

    private Integer id;

    private String user;
    private String content;

    private LocalDateTime date;


}
