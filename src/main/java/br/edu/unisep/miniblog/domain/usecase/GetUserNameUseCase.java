package br.edu.unisep.miniblog.domain.usecase;

import br.edu.unisep.miniblog.domain.user.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class GetUserNameUseCase {

    private static final String WS_GET_USER = "lb://miniblog-users-api/miniblog-users-api/user/";

    private final RestTemplate restTemplate;

    public String execute(Integer userId) {
        var response = restTemplate.getForEntity(WS_GET_USER + userId, UserDto.class);
        return response.getBody().getName();
    }
}
