package br.edu.unisep.miniblog.domain.usecase;

import br.edu.unisep.miniblog.domain.builder.PostBuilder;
import br.edu.unisep.miniblog.domain.dto.NewPostDto;
import br.edu.unisep.miniblog.model.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SavePostUseCase {

    private final PostRepository postRepository;
    private final PostBuilder builder;

    public void execute(NewPostDto post, Integer userId) {
        var newPost = builder.from(post, userId);
        postRepository.save(newPost);
    }
}
