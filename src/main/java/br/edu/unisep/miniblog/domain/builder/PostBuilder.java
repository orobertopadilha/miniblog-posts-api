package br.edu.unisep.miniblog.domain.builder;

import br.edu.unisep.miniblog.domain.dto.NewPostDto;
import br.edu.unisep.miniblog.domain.dto.PostDto;
import br.edu.unisep.miniblog.model.entity.Post;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PostBuilder {

    public PostDto from(Post post, String userName) {
        return new PostDto(post.getId(), userName, post.getContent(), post.getDate());
    }

    public List<PostDto> from(List<Post> posts, String userName) {
        return posts.stream().map(p -> from(p, userName)).collect(Collectors.toList());
    }

    public Post from(NewPostDto newPost, Integer userId) {
        var post = new Post();
        post.setContent(newPost.getContent());
        post.setDate(LocalDateTime.now());
        post.setContent(newPost.getContent());
        post.setUser(userId);

        return post;
    }

}
